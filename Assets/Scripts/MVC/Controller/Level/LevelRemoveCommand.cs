﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;

public class LevelRemoveCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        LevelsProxy levelsProxy = (LevelsProxy)ApplicationFacade.Instance.RetrieveProxy(LevelsProxy.NAME);
        Object.Destroy(levelsProxy.LevelsData.CurLevel.gameObject);
        levelsProxy.LevelsData.CurLevel = null;
    }
}
