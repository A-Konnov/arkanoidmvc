﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlatformData", menuName = "PlatformData")]
public class PlatformData : ScriptableObject
{
    [Header("Setup Platform")]
    [SerializeField] private float _speed = 2f;

    [Header("Prefab Platform")]
    [SerializeField] private GameObject platformPrefab;
    
    public GameObject CurPlatform { get; set; }
    
    public GameObject GetPlatformPrefab => platformPrefab;
    public float GetSpeed => _speed;
    
    
}
