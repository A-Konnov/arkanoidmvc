﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RecordPanel : MonoBehaviour
{
    private TextMeshProUGUI _recordText;

    private void Awake()
    {
        _recordText = GetComponent<TextMeshProUGUI>();
    }

    public void Change(int value)
    {
        _recordText.text = value.ToString();
    }
}
