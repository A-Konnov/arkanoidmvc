﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

public class BallsMediator : Mediator
{
    public const string NAME = "BallsPanelMediator";
    public BallsMediator(string mediatorName, object viewComponent = null) : base(mediatorName, viewComponent) { }
    
    private BallsCountPanel GetPanel => ViewComponent as BallsCountPanel;
    private BallsData _ballsData;

    public override string[] ListNotificationInterests()
    {
        List<string> listNotificationInterests = new List<string>();
        listNotificationInterests.Add(Notifications.GameStart);
        listNotificationInterests.Add(Notifications.LevelRespawn);
        listNotificationInterests.Add(Notifications.LevelNext);
        return listNotificationInterests.ToArray();
    }

    public override void OnRegister()
    {
        ViewComponent = Object.FindObjectOfType<BallsCountPanel>();
        var ballsProxy = (BallsProxy)ApplicationFacade.Instance.RetrieveProxy(BallsProxy.NAME);
        _ballsData = ballsProxy.BallsData;
    }

    public override void HandleNotification(INotification notification)
    {
        GetPanel.ShowBalls(_ballsData.CurBallsCount, _ballsData.GetBallPrefabUI);
    }
}
