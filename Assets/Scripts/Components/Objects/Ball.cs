﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
    private Rigidbody2D _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void Launching(Vector2 velocity)
    {
        _rb.velocity = Vector2.zero;
        _rb.AddForce(velocity, ForceMode2D.Impulse);
    }
}
