﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelsData", menuName = "LevelsData")]
public class LevelsData : ScriptableObject
{
    [SerializeField] private GameObject[] _levels;
    private Queue<GameObject> _queueLevels = null;
    
    public void CreateQueueLevels()
    {
        _queueLevels = new Queue<GameObject>();
        for (int i = 0; i < _levels.Length; i++)
        {
            _queueLevels.Enqueue(_levels[i]);
        }
    }
    
    public Level CurLevel { get; set; }
    
    public GameObject GetLevel()
    {
        if (_queueLevels.Count <= 0)
            return null;

        var newLevelPrefab = _queueLevels.Dequeue();
        return newLevelPrefab;

    }
}
