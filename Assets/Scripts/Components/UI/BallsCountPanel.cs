﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsCountPanel : MonoBehaviour
{
    public void ShowBalls(int count, GameObject item)
    {
        var curCount = transform.childCount;
        
        while (curCount < count)
        {
            Instantiate(item, transform);
            curCount++;
        }

        for (int i = 0; i < curCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(i < count);
        }
    }
}
