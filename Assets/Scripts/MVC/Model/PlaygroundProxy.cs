﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;
using System.IO;
using Object = UnityEngine.Object;

public class PlaygroundProxy : Proxy
{
    public const string NAME = "PlaygroundProxy";
    public PlaygroundProxy(string proxyName, object data = null) : base(proxyName, data) { }
    public Playground Playground
    {
        get
        {
            if (Data == null)
                Data = Object.FindObjectOfType<Playground>();
            
            return Data as Playground;
        }
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Debug.Log("PlaygroundProxy Registered");
    }
}

