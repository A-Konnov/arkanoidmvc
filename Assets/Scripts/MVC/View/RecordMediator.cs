﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

public class RecordMediator : Mediator
{
    public const string NAME = "RecordPanelMediator";
    public RecordMediator(string mediatorName, object viewComponent = null) : base(mediatorName, viewComponent) { }
    
    private RecordPanel GetPanel => ViewComponent as RecordPanel;
    private ScoreProxy _scoreProxy;

    public override string[] ListNotificationInterests()
    {
        List<string> listNotificationInterests = new List<string>();
        listNotificationInterests.Add(Notifications.GameStart);
        listNotificationInterests.Add(Notifications.RecordShow);
        return listNotificationInterests.ToArray();
    }

    public override void OnRegister()
    {
        ViewComponent = Object.FindObjectOfType<RecordPanel>();
        _scoreProxy = (ScoreProxy)ApplicationFacade.Instance.RetrieveProxy(ScoreProxy.NAME);
    }

    public override void HandleNotification(INotification notification)
    {
        GetPanel.Change(_scoreProxy.Record);
    }
}
