﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameEndPanel : MonoBehaviour
{
    [SerializeField] private Image _panel;
    [SerializeField] private Transform _infoTxt;

    private void Start()
    {
        _panel.gameObject.SetActive(false);
        _infoTxt.gameObject.SetActive(false);
    }

    public void Play()
    {
        _panel.gameObject.SetActive(true);
        _infoTxt.gameObject.SetActive(true);
        
        var startColor = _panel.color;
        var endValue = startColor.a;
        startColor.a = 0;
        _panel.color = startColor;
        
        _infoTxt.localScale = Vector3.zero;

        var seq = DOTween.Sequence();
        seq.Append(_panel.DOFade(endValue, 0.5f));
        seq.Append(_infoTxt.DOScale(Vector3.one, 1));
        seq.Play();
    }
}
