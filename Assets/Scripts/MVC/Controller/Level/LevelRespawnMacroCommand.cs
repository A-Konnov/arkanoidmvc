﻿using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using UnityEngine;
using PureMVC.Patterns.Command;

public class LevelRespawnMacroCommand : MacroCommand
{
    protected override void InitializeMacroCommand()
    {
        AddSubCommand(() => new BallRespawnCommand());
        AddSubCommand(() => new PlatformPlaceCommand());
    }
}
