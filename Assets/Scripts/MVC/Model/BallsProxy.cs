﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;
using System.IO;

public class BallsProxy : Proxy
{
    public const string NAME = "BallsProxy";
    public BallsProxy(string proxyName, object data = null) : base(proxyName, data) { }
    public BallsData BallsData
    {
        get
        {
            if (Data == null)
                Data = Resources.Load("BallsData") as BallsData;
            
            return Data as BallsData;
        }
    }
}

