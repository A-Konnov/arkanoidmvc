﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class BallRespawnCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        BallsProxy ballsProxy = (BallsProxy)ApplicationFacade.Instance.RetrieveProxy(BallsProxy.NAME);
        var data = ballsProxy.BallsData;
        data.CurBallsCount--;

        if (data.CurBallsCount > 0)
        {
            PlaygroundProxy playgroundProxy = (PlaygroundProxy)ApplicationFacade.Instance.RetrieveProxy(PlaygroundProxy.NAME);
            data.CurBall.transform.position = playgroundProxy.Playground.spawnPointBall.position;
            data.CurBall.Launching(data.GetVelocity);
        }
        else
        {
            SendNotification(Notifications.GameEnd);
        }
    }
}
