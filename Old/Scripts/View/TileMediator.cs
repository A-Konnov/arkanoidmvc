﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

public class TileMediator : Mediator
{
    public new const string NAME = "TileMediator";

    private Tile _tile
    {
        get { return ((GameObject) ViewComponent).GetComponent<Tile>(); }
    }

    private TileProxy _proxy;

    //IMediatorPlug needs
    public TileMediator(string mediatorName, object viewComponent) : base(mediatorName, viewComponent) { }
    
    public override string[] ListNotificationInterests()
    {
        string[] list = new string[1];
        list[0] = TileProxy.UPDATED;
        return list;
    }

    public override void HandleNotification(INotification notification)
    {
        switch (notification.Name)
        {
            case TileProxy.UPDATED:
                _tile.UpdateStrength(_proxy.Strength);
                break;
        }
    }

    public override void OnRegister()
    {
        Debug.Log("OnRegister:" + MediatorName);

        _proxy = Facade.RetrieveProxy(TileProxy.NAME) as TileProxy;
        Debug.Log(_proxy.GetHashCode());
        _proxy.Strength = _tile.Strength;
        _tile.OnClickAction = OnClick;
        _tile.UpdateStrength();
    }

    //Add count number
    void OnClick()
    {
        _proxy.Remove();
    }

    public override void OnRemove()
    {
        Debug.Log("Remove:" + MediatorName);
    }
}