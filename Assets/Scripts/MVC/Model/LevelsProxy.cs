﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;
using System.IO;

public class LevelsProxy : Proxy
{
    public const string NAME = "LevelsProxy";
    public LevelsProxy(string proxyName, object data = null) : base(proxyName, data) { }
    public LevelsData LevelsData
    {
        get
        {
            if (Data == null)
                Data = Resources.Load("LevelsData") as LevelsData;
            
            return Data as LevelsData;
        }
    }

    public override void OnRegister()
    {
        base.OnRegister();
        LevelsData.CreateQueueLevels();
        
        Debug.Log("LevelsProxy Registered");
    }
}

