﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Patterns.Command;
using PureMVC.Interfaces;

public class GameStartMacroCommand : MacroCommand
{
    protected override void InitializeMacroCommand()
    {
        AddSubCommand(() => new AssignCameraCommand());
        AddSubCommand(() => new LevelPlaceCommand());
        AddSubCommand(() => new PlatformPlaceCommand());
        AddSubCommand(() => new BallPlaceCommand());
    }
}

