﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Patterns.Command;
using PureMVC.Interfaces;

public class StartupCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        base.Execute(notification);
        GameObject gameStart = Object.FindObjectOfType<GameStart>().gameObject;
        if (gameStart == null)
        {
            Debug.LogError("GameStart is Null,Please check it");
            return;
        }

        Debug.Log("Startup completed");
    }
}

