﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;

public class StartUpCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        Debug.Log("Execute StartUpCommand");
        //Register default proxies and commands

        Facade.RegisterProxy(new TileProxy(TileProxy.NAME));
    }
}