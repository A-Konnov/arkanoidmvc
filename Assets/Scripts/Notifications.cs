﻿using System.Collections;
using System.Collections.Generic;


public static class Notifications
{
    //Framework
    public const string StartUp = "StartUp";
    public const string GameStart = "GameStart";
    public const string GameEnd = "GameEnd";
    
    //GamePlay
    public const string LevelNext = "LevelNext";
    public const string LevelRespawn = "LevelRespawn";
    public const string TileDestroy = "TileDestroy";
    public const string RecordReset = "RecordReset";
    
    //UI
    public const string ScoreShow = "ScoreShow";
    public const string RecordShow = "RecordShow";
}