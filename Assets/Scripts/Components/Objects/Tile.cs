﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private int _strength = 3;

    private void OnMouseDown()
    {
        _strength--;

        if (_strength <= 0)
        {
            ApplicationFacade.Instance.SendNotification(Notifications.TileDestroy, new TileDestroyCommand.Data(this));
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Ball"))
            return;
        
        _strength--;

        if (_strength <= 0)
        {
            ApplicationFacade.Instance.SendNotification(Notifications.TileDestroy, new TileDestroyCommand.Data(this));
            Destroy(gameObject);
        }
    }
}
