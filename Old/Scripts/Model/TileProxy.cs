﻿using UnityEngine;
using System.Collections;
using PureMVC.Patterns.Proxy;

public class TileProxy : Proxy
{
    public const string NAME = "TileProxy";
    public const string UPDATED = "TileProxy.Updated";

    private int _strength;

    public TileProxy(string proxyName): base(proxyName) { }

    public override void OnRegister()
    {
        Debug.Log("TileProxy OnRegister");
    }

    /// <summary>
    /// Called by the Model when the Proxy is removed
    /// </summary>
    public override void OnRemove() { }


    public int Strength
    {
        get => _strength;
        set => _strength = value;
    }

    public void Remove()
    {
        _strength--;
        SendNotification(UPDATED);
    }
}