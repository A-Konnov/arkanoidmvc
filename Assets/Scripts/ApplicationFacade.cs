﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns.Facade;


public class ApplicationFacade : Facade
{
    public static ApplicationFacade Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GetInstance(() => new ApplicationFacade());
            }

            return instance as ApplicationFacade;
        }
    }


    protected override void InitializeController()
    {
        base.InitializeController();
        RegisterCommand(Notifications.StartUp, () => new StartupCommand());
        RegisterCommand(Notifications.GameStart, () => new GameStartMacroCommand());
        RegisterCommand(Notifications.GameEnd, () => new GameEndMacroCommand());
        RegisterCommand(Notifications.LevelNext, () => new LevelNextMacroCommand());
        RegisterCommand(Notifications.LevelRespawn, () => new LevelRespawnMacroCommand());

        RegisterCommand(Notifications.TileDestroy, () => new TileDestroyCommand());
        RegisterCommand(Notifications.RecordReset, () => new RecordResetCommand());
    }

    protected override void InitializeModel()
    {
        base.InitializeModel();
        RegisterProxy(new PlaygroundProxy(PlaygroundProxy.NAME));
        RegisterProxy(new ScoreProxy(ScoreProxy.NAME));
        RegisterProxy(new LevelsProxy(LevelsProxy.NAME));
        RegisterProxy(new PlatformProxy(PlatformProxy.NAME));
        RegisterProxy(new BallsProxy(BallsProxy.NAME));
    }

    protected override void InitializeView()
    {
        base.InitializeView();
        RegisterMediator(new ScoreMediator(ScoreMediator.NAME));
        RegisterMediator(new RecordMediator(RecordMediator.NAME));
        RegisterMediator(new BallsMediator(BallsMediator.NAME));
        RegisterMediator(new GameEndMediator(GameEndMediator.NAME));
    }

    public void StartUpHandle()
    {
        SendNotification(Notifications.StartUp);
    }

    public void GameStartHandle()
    {
        SendNotification(Notifications.GameStart);
    }
}