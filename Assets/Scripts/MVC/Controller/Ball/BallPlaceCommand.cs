﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class BallPlaceCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        BallsProxy ballsProxy = (BallsProxy)ApplicationFacade.Instance.RetrieveProxy(BallsProxy.NAME);
        PlaygroundProxy playgroundProxy = (PlaygroundProxy)ApplicationFacade.Instance.RetrieveProxy(PlaygroundProxy.NAME);

        var data = ballsProxy.BallsData;
        data.AssignStartBallsCount();
        
        if (data.CurBall == null)
        {
            data.CurBall = Object.Instantiate(data.GetBallPrefab).GetComponent<Ball>();
        }
        
        data.CurBall.transform.position = playgroundProxy.Playground.spawnPointBall.position;
        data.CurBall.Launching(data.GetVelocity);
    }
}
