﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<Tile> _tiles;
    public int TilesCount => _tiles.Count;

    private void Start()
    {
        _tiles = new List<Tile>();
        GetComponentsInChildren(true, _tiles);
    }

    public void RemoveTile(Tile tile)
    {
        _tiles.Remove(tile);
    }

    public void RemoveLevel()
    {
        Destroy(gameObject);
    }
}
