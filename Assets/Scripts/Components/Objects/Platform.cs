﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider2D))]
public class Platform : MonoBehaviour
{
    [Serializable]
    private struct Border
    {
        private float _left;
        private float _right;

        
        public Border(float left, float right)
        {
            this._left = left;
            this._right = right;
        }

        public bool IsWithinBorders(float position)
        {
            return position > _left && position < _right;
        }

        public bool AtLeftEdge(float position)
        {
            return position <= _left;
        }

        public bool AtRightEdge(float position)
        {
            return position >= _right;
        }
    }

    private PlatformData _data;
    private Border _border;
    private Vector2 _speed;

    private void Awake()
    {
        _data = Resources.Load("PlatformData") as PlatformData;
    }

    private void Start()
    {
        _speed = new Vector2(_data.GetSpeed, 0);
        AssignBorder();
    }


    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && !_border.AtLeftEdge(transform.position.x))
        {
            transform.Translate(_speed * Time.deltaTime * -1);
        }
        else if (Input.GetKey(KeyCode.RightArrow) && !_border.AtRightEdge(transform.position.x))
        {
            transform.Translate(_speed * Time.deltaTime);
        }
    }

    private void AssignBorder()
    {
        var camera = Camera.main;
        var platformHalfSize = GetComponent<BoxCollider2D>().bounds.size.x * 0.5f;
        var cameraHalfSize = camera.orthographicSize * camera.aspect;
        var cameraPosition = camera.transform.position;
        var rightBorder = cameraPosition.x + cameraHalfSize - platformHalfSize;
        var leftBorder = rightBorder * -1;

        _border = new Border(leftBorder, rightBorder);
    }
}