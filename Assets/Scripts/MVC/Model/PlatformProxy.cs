﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;
using System.IO;

public class PlatformProxy : Proxy
{
    public const string NAME = "PlatformProxy";
    public PlatformProxy(string proxyName, object data = null) : base(proxyName, data) { }
    public PlatformData PlatformData
    {
        get
        {
            if (Data == null)
                Data = Resources.Load("PlatformData") as PlatformData;
            
            return Data as PlatformData;
        }
    }
}

