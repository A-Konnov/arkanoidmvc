﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BallsData", menuName = "BallsData")]
public class BallsData : ScriptableObject
{
    [Header("Setup Ball")]
    [SerializeField] private Vector2 _velocity;
    [SerializeField] private int _startBallsCount = 3;

    [Header("Prefab Ball")]
    [SerializeField] private GameObject _ballPrefab;

    [SerializeField] private GameObject _ballPrefabUI;
    
    public int CurBallsCount { get; set; }
    
    public Ball CurBall { get; set; }
    public GameObject GetBallPrefab => _ballPrefab;

    public GameObject GetBallPrefabUI => _ballPrefabUI;

    public Vector2 GetVelocity => _velocity;

    public void AssignStartBallsCount()
    {
        CurBallsCount = _startBallsCount;
    }
}
