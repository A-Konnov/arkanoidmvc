﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class PlatformRemoveCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        PlatformProxy platformProxy = (PlatformProxy)ApplicationFacade.Instance.RetrieveProxy(PlatformProxy.NAME);
        Object.Destroy(platformProxy.PlatformData.CurPlatform.gameObject);
    }
}
