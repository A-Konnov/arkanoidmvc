﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class TileDestroyCommand : SimpleCommand
{
    public class Data
    {
        public Tile tile;

        public Data(Tile tile)
        {
            this.tile = tile;
        }
    }
    
    public override void Execute(INotification notification)
    {
        Data data= notification.Body as Data;
        if (data == null)
            return;
        
        LevelsProxy levelsProxy = (LevelsProxy)ApplicationFacade.Instance.RetrieveProxy(LevelsProxy.NAME);
        var curLevel = levelsProxy.LevelsData.CurLevel;
        curLevel.RemoveTile(data.tile);
        
        ScoreProxy scoreProxy = (ScoreProxy)ApplicationFacade.Instance.RetrieveProxy(ScoreProxy.NAME);
        scoreProxy.AddScore();
        SendNotification(Notifications.ScoreShow);

        if (curLevel.TilesCount <= 0)
        {
            SendNotification(Notifications.LevelNext);
        }
    }
}
