﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Patterns.Command;
using PureMVC.Interfaces;

public class AssignCameraCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        PlaygroundProxy playgroundProxy = ApplicationFacade.Instance.RetrieveProxy(PlaygroundProxy.NAME) as PlaygroundProxy;
        var backSR = playgroundProxy.Playground.background.GetComponent<SpriteRenderer>();
        var camera = Camera.main;
        
        camera.orthographicSize = backSR.bounds.size.x / (camera.aspect * 2);

        var offset = (camera.orthographicSize * 2f - backSR.bounds.size.y) * 0.5f;
        var newPosCam = backSR.transform.position;
        newPosCam.z = -10f;
        newPosCam.y += offset;
        camera.transform.position = newPosCam;
    }
}
