﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ResetRecord : MonoBehaviour
{
    private Button _btn;

    private void Awake()
    {
        _btn = GetComponent<Button>();
    }

    private void Start()
    {
        _btn.onClick.AddListener(OnResetRecord);
    }

    private void OnResetRecord()
    {
        ApplicationFacade.Instance.SendNotification(Notifications.RecordReset);
    }

    private void OnDestroy()
    {
        _btn.onClick.RemoveListener(OnResetRecord);
    }
}
