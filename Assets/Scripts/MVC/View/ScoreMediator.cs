﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

public class ScoreMediator : Mediator
{
    public const string NAME = "ScorePanelMediator";
    public ScoreMediator(string mediatorName, object viewComponent = null) : base(mediatorName, viewComponent) { }
    
    private ScorePanel GetPanel => ViewComponent as ScorePanel;
    private ScoreProxy _scoreProxy;

    public override string[] ListNotificationInterests()
    {
        List<string> listNotificationInterests = new List<string>();
        listNotificationInterests.Add(Notifications.ScoreShow);
        return listNotificationInterests.ToArray();
    }

    public override void OnRegister()
    {
        ViewComponent = Object.FindObjectOfType<ScorePanel>();
        _scoreProxy = (ScoreProxy)ApplicationFacade.Instance.RetrieveProxy(ScoreProxy.NAME);
    }

    public override void HandleNotification(INotification notification)
    {
        switch (notification.Name)
        {
            case Notifications.ScoreShow:
                GetPanel.Change(_scoreProxy.Score);
                break;
        }
    }
}
