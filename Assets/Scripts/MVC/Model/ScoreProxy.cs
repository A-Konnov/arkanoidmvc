﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;
using System.IO;

public class ScoreProxy : Proxy
{
    public new static string NAME = "ScoreProxy";
    public ScoreProxy(string proxyName, object data = null) : base(proxyName, data) { }
    public int Score { get; set; }
    public int Record => PlayerPrefs.GetInt("Record");

    public override void OnRegister()
    {
        Score = 0;
    }

    public void AddScore()
    {
        Score++;
    }

    public void SaveRecord()
    {
        if (Score > Record)
            PlayerPrefs.SetInt("Record", Score);
    }

    public void ResetRecord()
    {
        PlayerPrefs.SetInt("Record", 0);
    }
}

