﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class GameEndMacroCommand : MacroCommand
{
    protected override void InitializeMacroCommand()
    {
        AddSubCommand(() => new RecordSaveCommand());
        AddSubCommand(() => new BallRemoveCommand());
        AddSubCommand(() => new PlatformRemoveCommand());
    }
}
