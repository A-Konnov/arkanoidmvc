﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class BallRemoveCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        BallsProxy ballsProxy = (BallsProxy)ApplicationFacade.Instance.RetrieveProxy(BallsProxy.NAME);
        Object.Destroy(ballsProxy.BallsData.CurBall.gameObject);
    }
}
