﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PureMVC.Core;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Patterns.Command;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using PureMVC.Patterns.Observer;
using PureMVC.Patterns.Proxy;


public class GameStart : MonoBehaviour
{
    private void Awake()
    {
        ApplicationFacade.Instance.StartUpHandle();
    }

    void Start()
    {
        ApplicationFacade.Instance.GameStartHandle();
    }

    private void OnDestroy()
    {
        ApplicationFacade.Instance.RemoveProxy(PlaygroundProxy.NAME);
        ApplicationFacade.Instance.RemoveProxy(ScoreProxy.NAME);
        ApplicationFacade.Instance.RemoveProxy(LevelsProxy.NAME);
        ApplicationFacade.Instance.RemoveProxy(PlatformProxy.NAME);
        ApplicationFacade.Instance.RemoveProxy(BallsProxy.NAME);
    }
}