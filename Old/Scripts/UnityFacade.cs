﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Facade;
using PureMVC.Patterns.Mediator;
using System;

public class UnityFacade : Facade
{
    public const string STARTUP = "UnityFacade.StartUp";

    static UnityFacade()
    {
        instance = new UnityFacade();
    }

    // Override Singleton Factory method 
    public static UnityFacade GetInstance()
    {
        return instance as UnityFacade;
    }

    protected override void InitializeController()
    {
        base.InitializeController();
        RegisterCommand(STARTUP, () => new StartUpCommand());
    }

    public void StartUp()
    {
        SendNotification(STARTUP);
    }

    //Handle IMediatorPlug connection
    public void ConnectMediator(IMediatorPlug item)
    {
        Type mediatorType = Type.GetType(item.GetClassRef());
        if (mediatorType != null)
        {
            IMediator mediatorPlug = (IMediator) Activator.CreateInstance(mediatorType, item.GetName(), item.GetView());
            RegisterMediator(mediatorPlug);
        }
    }

    public void DisconnectMediator(string mediatorName)
    {
        RemoveMediator(mediatorName);
    }
}