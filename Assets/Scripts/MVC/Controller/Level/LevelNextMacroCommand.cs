﻿using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using UnityEngine;
using PureMVC.Patterns.Command;

public class LevelNextMacroCommand : MacroCommand
{
    protected override void InitializeMacroCommand()
    {
        AddSubCommand(() => new LevelRemoveCommand());
        AddSubCommand(() => new LevelPlaceCommand());
        AddSubCommand(() => new PlatformPlaceCommand());
        AddSubCommand(() => new BallPlaceCommand());
    }
}
