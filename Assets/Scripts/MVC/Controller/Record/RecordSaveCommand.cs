﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;

public class RecordSaveCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        ScoreProxy scoreProxy = ApplicationFacade.Instance.RetrieveProxy(ScoreProxy.NAME) as ScoreProxy;
        scoreProxy.SaveRecord();
        SendNotification(Notifications.RecordShow);
    }
}
