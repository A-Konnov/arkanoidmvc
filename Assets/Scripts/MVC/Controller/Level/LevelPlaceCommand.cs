﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class LevelPlaceCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        LevelsProxy levelsProxy = (LevelsProxy)ApplicationFacade.Instance.RetrieveProxy(LevelsProxy.NAME);
        var newLevelPrefab = levelsProxy.LevelsData.GetLevel();

        if (newLevelPrefab == null)
        {
            SendNotification(Notifications.GameEnd);
            return;
        }

        PlaygroundProxy playgroundProxy = (PlaygroundProxy) ApplicationFacade.Instance.RetrieveProxy(PlaygroundProxy.NAME);        
        var newLevelGO = Object.Instantiate(newLevelPrefab, Vector3.zero, Quaternion.identity, playgroundProxy.Playground.transform);

        levelsProxy.LevelsData.CurLevel = newLevelGO.GetComponent<Level>();
    }
}
