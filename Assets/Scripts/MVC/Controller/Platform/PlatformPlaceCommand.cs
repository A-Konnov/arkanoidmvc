﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;


public class PlatformPlaceCommand : SimpleCommand
{
    public override void Execute(INotification notification)
    {
        PlatformProxy platformProxy = (PlatformProxy)ApplicationFacade.Instance.RetrieveProxy(PlatformProxy.NAME);
        PlaygroundProxy playgroundProxy = (PlaygroundProxy)ApplicationFacade.Instance.RetrieveProxy(PlaygroundProxy.NAME);
        
        if (platformProxy.PlatformData.CurPlatform == null)
        {
            platformProxy.PlatformData.CurPlatform = Object.Instantiate(platformProxy.PlatformData.GetPlatformPrefab);
        }

        platformProxy.PlatformData.CurPlatform.transform.position = playgroundProxy.Playground.spawnPointPlatform.position;
    }
}
