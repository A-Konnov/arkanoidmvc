﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

public class GameEndMediator : Mediator
{
    public const string NAME = "GameEndMediator";
    public GameEndMediator(string mediatorName, object viewComponent = null) : base(mediatorName, viewComponent) { }
    
    private GameEndPanel GetPanel => ViewComponent as GameEndPanel;

    public override string[] ListNotificationInterests()
    {
        List<string> listNotificationInterests = new List<string>();
        listNotificationInterests.Add(Notifications.GameEnd);
        return listNotificationInterests.ToArray();
    }

    public override void OnRegister()
    {
        ViewComponent = Object.FindObjectOfType<GameEndPanel>();
    }

    public override void HandleNotification(INotification notification)
    {
        GetPanel.Play();
    }
}
